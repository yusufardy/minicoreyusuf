package id.co.sigma.day4.model;

import java.util.Set;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Entity
//@Table(name = "nasabah")
public class Nasabah {
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@Column(name = "id_nasabah", nullable = false)
	private Long id;
	
//	@Column(name = "nama_nasabah", nullable = false, unique = false, length = 50)
	private String nama_nasabah;
	
//	@Column(name = "no_hp", nullable = false, unique = false, length = 50)
	private String no_hp;
	
//	@Column(name = "alamat", nullable = false, unique = false, length = 50)
	private String alamat;

	@JsonIgnore
//	@OneToMany(mappedBy = "nasabah")
	private Set<Rekening> rekening;

	public Nasabah() {
		super();
	}

	public Nasabah(Long id, String nama_nasabah, String no_hp, String alamat, Set<Rekening> rekening) {
		this.id = id;
		this.nama_nasabah = nama_nasabah;
		this.no_hp = no_hp;
		this.alamat = alamat;
		this.rekening = rekening;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama_nasabah() {
		return nama_nasabah;
	}

	public void setNama_nasabah(String nama_nasabah) {
		this.nama_nasabah = nama_nasabah;
	}

	public String getNo_hp() {
		return no_hp;
	}

	public void setNo_hp(String no_hp) {
		this.no_hp = no_hp;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Set<Rekening> getRekening() {
		return rekening;
	}

	public void setRekening(Set<Rekening> rekening) {
		this.rekening = rekening;
	}
	

}
