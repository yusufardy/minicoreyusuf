package id.co.sigma.request.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.sigma.request.feign.RekeningInterface;
import id.co.sigma.request.model.Nasabah;
//import id.co.sigma.request.repository.NasabahRepository;

@Service
public class NasabahService {
	@Autowired
	private RekeningInterface nasabahRepository;
	public List<Nasabah> findAll() {
		return nasabahRepository.getAllNasabah();
	}
	
	public ResponseEntity<Object> add(Nasabah nasabah){
		return nasabahRepository.add(nasabah);
	}

	public Nasabah findByID(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

//	public Nasabah findByID(Long id) {
//		return nasabahRepository.findById(id).get();
//	}

//	public void save(Nasabah nasabah) {
//		nasabahRepository.save(nasabah);
//	}

//	public void deleteByID(Long id) {
//		Nasabah nasabah = nasabahRepository.findById(id).get();
//		nasabahRepository.delete(nasabah);
//	}

}