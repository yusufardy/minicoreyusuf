package id.co.sigma.request.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.request.feign.RekeningInterface;
import id.co.sigma.request.model.Rekening;
//import id.co.sigma.request.repository.RekeningRepository;

@Service
public class RekeningService {
	@Autowired
	private RekeningService rekeningInterface;

	public List<Rekening> findAll() {
		return rekeningInterface.findAll();
	}

	public Rekening findByID(Long id) {
		return rekeningInterface.findByID(id);
	}

	public void save(Rekening rekening) {
		rekeningInterface.save(rekening);
	}

//	public void deleteByID(Long id) {
//		Rekening rekening = RekeningRepository.findById(id).get();
//		rekeningRepository.delete(rekening);
//	}

	public Rekening findByNoRekening(String noRekening) {
		return rekeningInterface.findByNoRekening(noRekening);
	}
}
