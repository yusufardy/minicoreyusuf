package id.co.sigma.request.feign;


import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import id.co.sigma.request.model.Rekening;
import id.co.sigma.request.model.Nasabah;

@FeignClient("NasabahRegistration")
public interface RekeningInterface {
	@RequestMapping(path="/account/get/all")
	List<Nasabah> getAllNasabah();

	@RequestMapping(path = "/account/add")
	ResponseEntity<Object> add(@RequestBody Nasabah nasabah);
	
	@RequestMapping(path = "/account/update")
	ResponseEntity<Object> update(@RequestBody Nasabah nasabah);
	
	@RequestMapping(path = "/account/inquiry/{id}/all")
	List<Rekening> inquiryAll(@PathVariable Long id);

	Object findById(Long id);


}
