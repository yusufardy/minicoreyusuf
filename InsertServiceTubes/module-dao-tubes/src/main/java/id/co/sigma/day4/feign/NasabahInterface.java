package id.co.sigma.day4.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import id.co.sigma.day4.model.Nasabah;

@FeignClient("NasabahRegistration")
public interface NasabahInterface {	
	
	@RequestMapping(path="/nasabah/account/get/all")
	List<Nasabah> getAllNasabah();

	@RequestMapping(path = "/nasabah/account/add")
	ResponseEntity<Object> add(@RequestBody Nasabah nasabah);


}
