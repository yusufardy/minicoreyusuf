package id.co.sigma.day4.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.day4.model.Nasabah;
import id.co.sigma.day4.model.Rekening;
import id.co.sigma.day4.request.RequestBalance;
import id.co.sigma.day4.request.RequestDeposit;
import id.co.sigma.day4.request.RequestWithdrawl;
import id.co.sigma.day4.request.TransferFrom;
import id.co.sigma.day4.response.Error;
import id.co.sigma.day4.response.ResponseBalance;
import id.co.sigma.day4.response.ResponseDeposit;
import id.co.sigma.day4.response.ResponseWithdrawl;
import id.co.sigma.day4.service.NasabahService;
import id.co.sigma.day4.service.RekeningService;

@RestController
public class Controller {
	@Autowired
	private NasabahService nasabahService;

//	@Autowired
//	private RekeningService rekeningService;

	// Untuk Nasabah

////////////////// Yusuf ////////////////
	
//	@RequestMapping(path="/account/get/all")
//	public List<Nasabah> getAllNasabah() {
//		return nasabahService.findAll();
//	}
	
	
	@RequestMapping(path = "/nasabah/account/add")
	public ResponseEntity<Object> add(@RequestBody Nasabah nasabah){
		return nasabahService.add(nasabah);
	}
//	
//	// exception masih blm OK di postmannya
//	@RequestMapping(path = "/account/add")
//	public ResponseEntity<Object> add(@RequestBody Nasabah nasabah) {
//		try {
//			nasabahService.save(nasabah);
//			Error eError = new Error(HttpStatus.ACCEPTED, "SUCCESS", " ");
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		} catch (DataIntegrityViolationException ex) {
//			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "FAILED", ex.getLocalizedMessage());
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		} catch (NullPointerException ne) {
//			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", ne.getLocalizedMessage());
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		}
//	}

	// exception masih blm OK di postmannya
//	@RequestMapping(path = "/account/update")
//	@ExceptionHandler(value = { Exception.class })
//	public ResponseEntity<Object> update(@RequestBody Nasabah nasabah) {
//		Nasabah pemilik = nasabahService.findByID(nasabah.getId());
//		try {
//			if (nasabah.getNama_nasabah() == null || pemilik.getNama_nasabah() == nasabah.getNama_nasabah()) {
//				nasabah.setNama_nasabah(pemilik.getNama_nasabah());
//			} else if (nasabah.getNo_hp() == null || pemilik.getNo_hp() == nasabah.getNo_hp()) {
//				nasabah.setNo_hp(pemilik.getNo_hp());
//			} else if (nasabah.getAlamat() == null || pemilik.getAlamat() == nasabah.getAlamat()) {
//				nasabah.setAlamat(pemilik.getAlamat());
//			} else {
//				Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "FAILED", " ");
//				return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//			}
//			nasabahService.save(nasabah);
//			Error eError = new Error(HttpStatus.ACCEPTED, "SUCCESS", " ");
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		} catch (NullPointerException ne) {
//			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", ne.getLocalizedMessage());
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		}
//		}

//	- inquiry data nasabah(tampilkan seluruh rekening yg dimiliki oleh nasabah dengan id yang di input di path)
//	path = /minicore/account/inquiry/{id}/all, 
//	response pasti berbentuk array json rekening

//	@RequestMapping(path = "/account/inquiry/{id}/all")
//	public List<Rekening> inquiryAll(@PathVariable Long id) {
//		Nasabah nasabah = nasabahService.findByID(id);
//		List<Rekening> rekening = new ArrayList<Rekening>();
//		rekening.addAll(nasabah.getRekening());
//		return rekening;
//	}

	// Untuk Rekening

//	- cek saldo						
//	path = /minicore/account/balance 	
//	req : {"account" : "89911100"}, 
//	res : {"balance" : "50000"}

//	@RequestMapping(path = "/account/balance")
//	public ResponseBalance cekSaldo(@RequestBody RequestBalance request) {
//		Rekening rekening = rekeningService.findByNoRekening(request.getNoRekening());
//		ResponseBalance response = new ResponseBalance();
//		response.setSaldo(rekening.getSaldo().toString());
//		return response;
//	}

////////////////// Widya/ Icha ////////////////		
	
//	- setor tunai
//	path = /minicore/transaction/deposit	
//	req = { "rekening" : "89xxxxx", "amount" : "1000000" }, 
//	res = { "rekening" : "89xxxxx", "saldo" : "500000000" }

//	@RequestMapping(path = "/transaction/deposit")
//	public ResponseDeposit setorTunai(@RequestBody RequestDeposit request) {
//		Rekening rekening = rekeningService.findByNoRekening(request.getNoRekening());
//		BigDecimal newSaldo = rekening.getSaldo();
//		rekening.setSaldo(newSaldo.add(new BigDecimal(request.getAmount())));
//		rekeningService.save(rekening);
//		ResponseDeposit response = new ResponseDeposit();
//		response.setAmount(newSaldo.toString());
//		response.setNoRekening(rekening.getNo_rekening());
//		return response;
//	}

//	- tarik tunai				
//	path = /minicore/transaction/withdrawal	
//	req = { "rekening" : "89xxxxx", "amount" : "1000000" }, 
//	res = { "rekening" : "89xxxxx", "saldo" : "400000000" }

//	@RequestMapping(path = "/transaction/withdrawl")
//	public ResponseWithdrawl withDrawl(@RequestBody RequestWithdrawl request) {
//		Rekening rekening = rekeningService.findByNoRekening(request.getNoRekening());
//		BigDecimal newSaldo = rekening.getSaldo();
//		rekening.setSaldo(newSaldo.subtract(new BigDecimal(request.getAmount())));
//		rekeningService.save(rekening);
//
//		ResponseWithdrawl response = new ResponseWithdrawl();
//		response.setAmount(newSaldo.toString());
//		response.setNoRekening(rekening.getNo_rekening());
//		return response;
//	}

//	@Transactional
//	- transfer antar nasabah		
//	path = /minicore/transaction/transfer	
//	req = { "rekeningAsal" : "89xxxxx", "rekeningTujuan" : "99xxxxx", "amount" : "1000000" }, 
//	res = {"message" : "SUCCESS/FAILED/ERROR"}

	// exception masih blm OK di postmannya
//	@Transactional
//	@RequestMapping(path = "/transaction/transfer")
//	public ResponseEntity<Object> transfer(@RequestBody TransferFrom request) {
//		try {
//			Rekening rekeningAsal = rekeningService.findByNoRekening(request.getFromNoRekening());
//			Rekening rekeningTujuan = rekeningService.findByNoRekening(request.getToNoRekening());
//
//			BigDecimal saldoAsal = rekeningAsal.getSaldo();
//			BigDecimal saldoTujuan = rekeningTujuan.getSaldo();
//			if (saldoAsal.compareTo(saldoTujuan) >= saldoTujuan.longValue()) {
//				rekeningAsal.setSaldo(saldoAsal.subtract(new BigDecimal(request.getAmount())));
//				rekeningTujuan.setSaldo(saldoTujuan.add(new BigDecimal(request.getAmount())));
//
//				rekeningService.save(rekeningAsal);
//				rekeningService.save(rekeningTujuan);
//				Error eError = new Error(HttpStatus.ACCEPTED, "SUCCESS", " ");
//				return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//			} else {
//				Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "FAILED", " ");
//				return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//			}
//		} catch (NullPointerException ne) {
//			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", ne.getLocalizedMessage());
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		}
//
//	}
}
