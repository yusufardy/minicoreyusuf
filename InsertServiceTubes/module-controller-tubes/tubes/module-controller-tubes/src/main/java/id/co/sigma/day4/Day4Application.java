package id.co.sigma.day4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@RefreshScope
@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationProperties
@EnableEurekaClient
@EnableFeignClients
public class Day4Application {

	public static void main(String[] args) {
		SpringApplication.run(Day4Application.class, args);
	}

}
