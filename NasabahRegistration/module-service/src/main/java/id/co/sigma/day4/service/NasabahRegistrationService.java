package id.co.sigma.day4.service;

import java.util.List;


import org.hibernate.annotations.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import id.co.sigma.day4.model.Nasabah;
import id.co.sigma.day4.repository.NasabahRepository;

@Service
public class NasabahRegistrationService {
	@Autowired
	private NasabahRepository nasabahRepository;
	
		@Cacheable(value = "NasabahRegistration.Nasabah.findAll",unless = "#result == null")
		public List<Nasabah> findAll() {
		return nasabahRepository.findAll();
	}
			@Caching(evict = {
					@CacheEvict(value = "NasabahRegistration.Nasabah.findAll",allEntries = true,beforeInvocation = true)
			})

	public Nasabah findByID(Long id) {
		return nasabahRepository.findById(id).get();
	}

	public void save(Nasabah nasabah) {
		nasabahRepository.save(nasabah);
	}

	public void deleteByID(Long id) {
		Nasabah nasabah = nasabahRepository.findById(id).get();
		nasabahRepository.delete(nasabah);
	}

}