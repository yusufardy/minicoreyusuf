package id.co.sigma.day4.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.day4.model.Nasabah;
import id.co.sigma.day4.model.Rekening;
import id.co.sigma.day4.service.NasabahRegistrationService;
//import id.co.sigma.request.request.RequestWithdrawl;
//import id.co.sigma.request.response.ResponseWithdrawl;
import id.co.sigma.day4.Error;

@RestController
public class NasabahRegistrationController {
	@Autowired
	private NasabahRegistrationService nasabahService;


	// Untuk Nasabah

	@RequestMapping(path="/account/get/all")
	public List<Nasabah> getAllNasabah() {
		return nasabahService.findAll();
	}
	
	// exception masih blm OK di postmannya
	@RequestMapping(path = "/account/add")
	public ResponseEntity<Object> add(@RequestBody Nasabah nasabah) {
		try {
			nasabahService.save(nasabah);
			Error eError = new Error(HttpStatus.ACCEPTED, "SUCCESS", " ");
			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
		} catch (DataIntegrityViolationException ex) {
			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "FAILED", ex.getLocalizedMessage());
			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
		} catch (NullPointerException ne) {
			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", ne.getLocalizedMessage());
			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
		}
	}

	// exception masih blm OK di postmannya
	@RequestMapping(path = "/account/update")
	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> update(@RequestBody Nasabah nasabah) {
		Nasabah pemilik = nasabahService.findByID(nasabah.getId());
		try {
			if (nasabah.getNama_nasabah() == null || pemilik.getNama_nasabah() == nasabah.getNama_nasabah()) {
				nasabah.setNama_nasabah(pemilik.getNama_nasabah());
			} else if (nasabah.getNo_hp() == null || pemilik.getNo_hp() == nasabah.getNo_hp()) {
				nasabah.setNo_hp(pemilik.getNo_hp());
			} else if (nasabah.getAlamat() == null || pemilik.getAlamat() == nasabah.getAlamat()) {
				nasabah.setAlamat(pemilik.getAlamat());
			} else {
				Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "FAILED", " ");
				return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
			}
			nasabahService.save(nasabah);
			Error eError = new Error(HttpStatus.ACCEPTED, "SUCCESS", " ");
			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
		} catch (NullPointerException ne) {
			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", ne.getLocalizedMessage());
			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
		}
	}

//	- inquiry data nasabah(tampilkan seluruh rekening yg dimiliki oleh nasabah dengan id yang di input di path)
//	path = /minicore/account/inquiry/{id}/all, 
//	response pasti berbentuk array json rekening

	@RequestMapping(path = "/account/inquiry/{id}/all")
	public List<Rekening> inquiryAll(@PathVariable Long id) {
		Nasabah nasabah = nasabahService.findByID(id);
		List<Rekening> rekening = new ArrayList<Rekening>();
		rekening.addAll(nasabah.getRekening());
		return rekening;
	}
	
//	- tarik tunai				
//	path = /minicore/transaction/withdrawal	
//	req = { "rekening" : "89xxxxx", "amount" : "1000000" }, 
//	res = { "rekening" : "89xxxxx", "saldo" : "400000000" }

//	@RequestMapping(path = "/nasabah/transaction/withdrawl")
//	public ResponseWithdrawl withDrawl(@RequestBody RequestWithdrawl request) {
//		Rekening rekening = Rekening.findByNoRekening(request.getNoRekening());
//		BigDecimal newSaldo = rekening.getSaldo();
//		rekening.setSaldo(newSaldo.subtract(new BigDecimal(request.getAmount())));
//		rekeningService.save(rekening);
//
//		ResponseWithdrawl response = new ResponseWithdrawl();
//		response.setAmount(newSaldo.toString());
//		response.setNoRekening(rekening.getNo_rekening());
//		return response;
//	}

}
