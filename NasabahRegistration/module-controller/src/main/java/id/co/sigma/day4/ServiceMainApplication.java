package id.co.sigma.day4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@RefreshScope
@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationProperties
@EnableEurekaClient
public class ServiceMainApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceMainApplication.class, args);
	}

}
