package id.co.sigma.day4;

	 
public class TransferFrom{
	     
	    String fromNoRekening;
	    String toNoRekening;
	    String amount;
		public String getFromNoRekening() {
			return fromNoRekening;
		}
		public void setFromNoRekening(String fromNoRekening) {
			this.fromNoRekening = fromNoRekening;
		}
		public String getToNoRekening() {
			return toNoRekening;
		}
		public void setToNoRekening(String toNoRekening) {
			this.toNoRekening = toNoRekening;
		}
		public String getAmount() {
			return amount;
		}
		public void setAmount(String amount) {
			this.amount = amount;
		}
}
