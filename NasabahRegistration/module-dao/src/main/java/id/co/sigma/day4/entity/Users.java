package id.co.sigma.day4.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;


@Entity
@Table(name = "USERS_AUTH")
public class Users implements UserDetails,Serializable {

    private Long userId;
    private String username;
    private String password;
    private String salary;
    private String age;
    private Set<Role> roles = new HashSet<Role>();
    private boolean enabled = false;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getUserId() {
        return userId;
    }


    @Column(name = "USER_LOGIN", nullable = false, unique = true)
    public String getUsername() {
        return username;
    }

    @Column(name = "PASSWORD", nullable = false)
    public String getPassword() {
        return password;
    }


    
    @Transient
    public Set<GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new LinkedHashSet<GrantedAuthority>();
        //noinspection CollectionAddAllCanBeReplacedWithConstructor
        authorities.addAll(roles);
        return authorities;
    }
    
    @Column(name = "ACCOUNT_NON_EXPIRED", nullable = false)
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

   
    @Column(name = "ACCOUNT_NON_LOCKED", nullable = false)
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

   
    @Column(name = "CREDENTIAL_NON_EXPIRED", nullable = false)
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    
    @Column(name = "ACCOUNT_ENABLED", nullable = false)
    public boolean isEnabled() {
        return enabled;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "USER_ROLES_AUTH", joinColumns = {
            @JoinColumn(name = "userId") }, inverseJoinColumns = @JoinColumn(name = "roleId"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }
}
