package id.co.sigma.day4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.day4.model.Rekening;

public interface RekeningRepository extends JpaRepository<Rekening, Long>{
	
	public Rekening findByNoRekening(String noRekening);
}
