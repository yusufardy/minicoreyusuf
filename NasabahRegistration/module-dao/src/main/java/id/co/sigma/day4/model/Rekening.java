package id.co.sigma.day4.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "rekening")
public class Rekening {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rekening")
	private Long id;
	
	@Column(name = "noRekening", nullable = false, unique = true)
	private String noRekening;
	
	@Column(nullable = false, unique = false)
	private BigDecimal saldo;
	
	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_nasabah")
	private Nasabah nasabah;
	
	public Rekening() {
		super();
	}

	public Rekening(Long id, String no_rekening, BigDecimal saldo, Nasabah nasabah) {
		this.id= id;
		this.noRekening = no_rekening;
		this.saldo = saldo;
		this.nasabah = nasabah;
	}

	public Long getId() {
		return id;
	}

	public void setId_rekening(Long id) {
		this.id = id;
	}

	public String getNo_rekening() {
		return noRekening;
	}

	public void setNo_rekening(String no_rekening) {
		this.noRekening = no_rekening;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public Nasabah getNasabah() {
		return nasabah;
	}

	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}

	public static Rekening findBy(Rekening noRekening2) {
		return noRekening2;
	}

	public static Rekening findByNoRekening(Rekening noRekening2) {
		return noRekening2;
	}
	
}
