package id.co.sigma.hazelcastserver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;

@Configuration
public class HazelcastConfig {

	@Bean
	public Config config() {
		return new ClasspathXmlConfig("app-hazelcast.xml");
	}
}
