package id.co.sigma.day4.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.day4.model.Nasabah;
import id.co.sigma.day4.repository.NasabahRepository;

@Service
public class NasabahRegistrationService {
	@Autowired
	private NasabahRepository nasabahRepository;

	public List<Nasabah> findAll() {
		return nasabahRepository.findAll();
	}

	public Nasabah findByID(Long id) {
		return nasabahRepository.findById(id).get();
	}

	public void save(Nasabah nasabah) {
		nasabahRepository.save(nasabah);
	}

	public void deleteByID(Long id) {
		Nasabah nasabah = nasabahRepository.findById(id).get();
		nasabahRepository.delete(nasabah);
	}

}