package id.co.sigma.day4;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

public class Error {
    private HttpStatus status;
    private String message;
    private List<String> errors;
    private String error;
  
	public Error(HttpStatus status, String message, List<String> errors) {
    	super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }
 
    public Error(HttpStatus status, String message, String error) {
    	super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
    
    
}
