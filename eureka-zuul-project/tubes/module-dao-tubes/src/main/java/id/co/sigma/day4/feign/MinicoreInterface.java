package id.co.sigma.day4.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("nasabah")
public interface MinicoreInterface {
	
	@RequestMapping(value = "/nasabah/internal/userCheck", method = {RequestMethod.POST});
	String login(@RequestParam String username, @RequestParam String password);
	
	
}
